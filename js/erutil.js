
function doValidate_erAddForm() {
    var form = $("#erAddForm");
    form.validate({
        rules:{
            // Business name required
            // Business name length 2 s- 20 chars
            erBusinessName:{
                required: true,
                rangelength: [2,20]
            },

            // Reviewer email required
            // Reviewer email is valid email address
            erReviewerEmail: {
                required: true,
                email: true
            },

            // Review date required
            erReviewDate: {
                required: true
            },

            // If 'Do you want to add your ratings' is checked

            // Food Quality must be 0 - 5
            erFoodQualityRating:{
                range: [0,5]
            },
            // Service must be 0 - 5
            erServiceRating:{
                range: [0,5]
            },
            // Value must be 0 - 5
            erValueRating:{
                range: [0,5]
            }
        },
        messages:{
            erBusinessName:{
                required: "You must provide a business name.",
                rangelength:"Business name must be 2 to 20 characters in length."
            },
            erReviewerEmail:{
                required: "A valid email is required.",
                email: "The email in invalid"
            },
            erReviewDate:{
                required: "Review date is required."
            },
            erFoodQualityRating:{
                range: "This rating must be an integer from 0 to 5."
            },
            erServiceRating:{
                required: "This rating must be an integer from 0 to 5."
            },
            erValueRating:{
                required: "This rating must be an integer from 0 to 5."
            }
        }
    });
    return form.valid();
}

function doValidate_erEditForm() {
    var form = $("#erEditForm");
    form.validate({
        rules:{
            // Business name required
            // Business name length 2 s- 20 chars
            erEditBusinessName:{
                required: true,
                rangelength: [2,20]
            },

            // Reviewer email required
            // Reviewer email is valid email address
            erEditReviewerEmail: {
                required: true,
                email: true
            },

            // Review date required
            erEditReviewDate: {
                required: true
            },

            // If 'Do you want to add your ratings' is checked

            // Food Quality must be 0 - 5
            erEditFoodQualityRating:{
                range: [0,5]
            },
            // Service must be 0 - 5
            erEditServiceRating:{
                range: [0,5]
            },
            // Value must be 0 - 5
            erEditValueRating:{
                range: [0,5]
            }
        },
        messages:{
            erEditBusinessName:{
                required: "You must provide a business name.",
                rangelength:"Business name must be 2 to 20 characters in length."
            },
            erEditReviewerEmail:{
                required: "A valid email is required.",
                email: "The email in invalid"
            },
            erEditReviewDate:{
                required: "Review date is required."
            },
            erEditFoodQualityRating:{
                range: "This rating must be an integer from 0 to 5."
            },
            erEditServiceRating:{
                required: "This rating must be an integer from 0 to 5."
            },
            erEditValueRating:{
                required: "This rating must be an integer from 0 to 5."
            }
        }
    });
    return form.valid();
}
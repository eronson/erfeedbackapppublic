
function erSaveButton_click() {
    checkSaveValidation();
}

function checkSaveValidation() {
    if (doValidate_erAddForm()) {
        console.info("Validation successful");
    }
    else{
        console.error("Validation failed");
    }
}

function erUpdateButton_click() {
    checkUpdateValidation();
}

function checkUpdateValidation() {
    if (doValidate_erEditForm()) {
        console.info("Validation successful");
    }
    else{
        console.error("Validation failed");
    }
}


function CalculateOverallRating() {
    // calculate the age
    var sumOfRatings = parseInt($("#erFoodQualityRating").val(),10) + parseInt($("#erServiceRating").val(),10) + parseInt($("#erValueRating").val(),10);

    var overallRating = ( sumOfRatings * (100/15));
    // show the Overall Rating
    $("#erOverallRating").val(overallRating.toFixed(0) + '%');
}

function CalculateEditOverallRating() {
    // calculate the age
    var sumOfRatings = parseInt($("#erEditFoodQualityRating").val(),10) + parseInt($("#erEditServiceRating").val(),10) + parseInt($("#erEditValueRating").val(),10);

    var overallRating = ( sumOfRatings * (100/15));
    // show the Overall Rating
    $("#erEditOverallRating").val(overallRating.toFixed(0) + '%');
}

function erRatingsDiv() {
    if ($('#erAddRatings').prop('checked')){
        $('#RatingsDiv').show();
        $("#erFoodQualityRating").val(0);
        $("#erServiceRating").val(0);
        $("#erValueRating").val(0);
        CalculateOverallRating();
    }
    else {
        $('#RatingsDiv').hide();
    }
}

function erEditRatingsDiv() {
    if ($('#erEditAddRatings').prop('checked')){
        $('#EditRatingsDiv').show();
        $("#erEditFoodQualityRating").val(0);
        $("#erEditServiceRating").val(0);
        $("#erEditValueRating").val(0);
        CalculateEditOverallRating();
    }
    else {
        $('#EditRatingsDiv').hide();
    }
}

function erInit() {
    console.info("DOM is ready");
    if (!$('#erAddRatings').prop('checked')) {
        erRatingsDiv();
    }
    if (!$('#erEditAddRatings').prop('checked')) {
        erEditRatingsDiv();
    }
    $("#erSaveButton").on("click", erSaveButton_click);
    $("#erAddRatings").on("click", erRatingsDiv);
    $("#erFoodQualityRating").on("change", CalculateOverallRating);
    $("#erServiceRating").on("change", CalculateOverallRating);
    $("#erValueRating").on("change", CalculateOverallRating);

    $("#erUpdateButton").on("click", erUpdateButton_click);
    $("#erEditAddRatings").on("click", erEditRatingsDiv);
    $("#erEditFoodQualityRating").on("change", CalculateEditOverallRating);
    $("#erEditServiceRating").on("change", CalculateEditOverallRating);
    $("#erEditValueRating").on("change", CalculateEditOverallRating);
}

$(document).ready(function () {
    erInit();
});